import 'dart:io';

class Menu {
  var mainMenu = [];
  var typeDrink = [];
  var sweet = [];
  var drinkPrice = [];
  int priceMenu = 0;

  showMenu() {
    print(
        'Menu of Taobin\n1.Recommended\n2.Tea\n3.Cocoa\n4.Protein shake\n5.Soda and other\n6.Coffee');
    print('Select your menu: ');
    int? numMenu = int.parse(stdin.readLineSync()!);
    if (numMenu == 1) {
      print(recomdedMenu());
      print(sweetMenu());
    } else if (numMenu == 2) {
      print(menuTea());
      print(sweetMenu());
    } else if (numMenu == 3) {
      print(menuCocoa());
      print(sweetMenu());
    } else if (numMenu == 4) {
      print(menuProtein());
      print(sweetMenu());
    } else if (numMenu == 5) {
      print(menuSodaOther());
      print(sweetMenu());
    } else if (numMenu == 6) {
      print(menuCoffee());
      print(sweetMenu());
    }
  }

  sweetMenu() {
    print('--------------------');
    print(
        'Select level of sweet\n1.Sweetless\n2.Normal Sweet\n3.Very Sweet\n4.Heavy Metal Sweet');
    print('Select of your level of sweet: ');
    int? numSweet = int.parse(stdin.readLineSync()!);
    if (numSweet == 1) {
      print('level of your sweet is Sweetless');
    } else if (numSweet == 2) {
      print('level of your sweet is Normal Sweet');
    } else if (numSweet == 3) {
      print('level of your sweet is Very Sweet');
    } else if (numSweet == 4) {
      print('level of your sweet is Heavy Metal Sweet');
    }
  }

  String recomdedMenu() {
    var price = [35, 40, 45, 50];
    var menu01 = 'Milk tea';
    var menu02 = 'Thai tea';
    var menu03 = 'Oreo Milk Bear';
    var menu04 = 'Cocoa';
    print('Recommended Menu\n1.' +
        menu01 +
        ' ' +
        price[0].toString() +
        ' bath\n2.' +
        menu02 +
        ' ' +
        price[1].toString() +
        ' bath\n3.' +
        menu03 +
        ' ' +
        price[2].toString() +
        ' bath\n4.' +
        menu04 +
        ' ' +
        price[3].toString() +
        ' bath');
    print('Select your order is: ');
    String recomd = stdin.readLineSync()!;
    if (recomd == "1") {
      return menu01 + " " + price[0].toString() + " " + " bath";
    } else if (recomd == "2") {
      return menu02 + " " + price[1].toString() + " " + " bath";
    } else if (recomd == "3") {
      return menu03 + " " + price[2].toString() + " " + " bath";
    } else if (recomd == "4") {
      return menu04 + " " + price[3].toString() + " " + " bath";
    }
    print('-------------------------------');
    return "error taobin";
  }

  String menuTea() {
    var price = [35, 40, 45, 60];
    var tea01 = 'Original Tea';
    var tea02 = 'Thai Tea';
    var tea03 = 'Black Tea';
    var tea04 = 'Lemon Tea';
    print('Recommended Menu\n1.' +
        tea01 +
        ' ' +
        price[0].toString() +
        ' bath\n2.' +
        tea02 +
        ' ' +
        price[1].toString() +
        ' bath\n3.' +
        tea03 +
        ' ' +
        price[2].toString() +
        ' bath\n4.' +
        tea04 +
        ' ' +
        price[3].toString() +
        ' bath');
    print('Select your order is: ');
    String recomd = stdin.readLineSync()!;
    if (recomd == "1") {
      return tea01 + " " + price[0].toString() + " " + " bath";
    } else if (recomd == "2") {
      return tea02 + " " + price[1].toString() + " " + " bath";
    } else if (recomd == "3") {
      return tea03 + " " + price[2].toString() + " " + " bath";
    } else if (recomd == "4") {
      return tea04 + " " + price[3].toString() + " " + " bath";
    }
    print('-------------------------------');
    return "error taobin";
  }

  String menuCocoa() {
    var price = [30, 40];
    var cocoa01 = 'Cocoa';
    var cocoa02 = 'Hot Cocoa';
    print('Recommended Menu\n1.' +
        cocoa01 +
        ' ' +
        price[0].toString() +
        ' bath\n2.' +
        cocoa02 +
        ' ' +
        price[1].toString() +
        ' bath');
    print('Select your order is: ');
    String recomd = stdin.readLineSync()!;
    if (recomd == "1") {
      return cocoa01 + " " + price[0].toString() + " " + " bath";
    } else if (recomd == "2") {
      return cocoa02 + " " + price[1].toString() + " " + " bath";
    }
    print('-------------------------------');
    return "error taobin";
  }

  String menuProtein() {
    var price = [100, 120, 250, 500];
    var pro1 = 'Matcha Protein';
    var pro2 = 'Taiwan Protein';
    var pro3 = 'Cocoa Protein';
    var pro4 = 'Original Protein';
    print('Recommended Menu\n1.' +
        pro1 +
        ' ' +
        price[0].toString() +
        ' bath\n2.' +
        pro2 +
        ' ' +
        price[1].toString() +
        ' bath\n3.' +
        pro3 +
        ' ' +
        price[2].toString() +
        ' bath\n4.' +
        pro4 +
        ' ' +
        price[3].toString() +
        ' bath');
    print('Select your order is: ');
    String recomd = stdin.readLineSync()!;
    if (recomd == "1") {
      return pro1 + " " + price[0].toString() + " " + " bath";
    } else if (recomd == "2") {
      return pro2 + " " + price[1].toString() + " " + " bath";
    } else if (recomd == "3") {
      return pro3 + " " + price[2].toString() + " " + " bath";
    } else if (recomd == "4") {
      return pro4 + " " + price[3].toString() + " " + " bath";
    }
    print('-------------------------------');
    return "error taobin";
  }

  String menuSodaOther() {
    var price = [25, 35, 35, 40];
    var soda1 = 'Soda';
    var soda2 = 'Strawberry Soda';
    var soda3 = 'Blueberry Soda';
    var soda4 = 'Lemon Soda';
    print('Recommended Menu\n1.' +
        soda1 +
        ' ' +
        price[0].toString() +
        ' bath\n2.' +
        soda2 +
        ' ' +
        price[1].toString() +
        ' bath\n3.' +
        soda3 +
        ' ' +
        price[2].toString() +
        ' bath\n4.' +
        soda4 +
        ' ' +
        price[3].toString() +
        ' bath');
    print('Select your order is: ');
    String recomd = stdin.readLineSync()!;
    if (recomd == "1") {
      return soda1 + " " + price[0].toString() + " " + " bath";
    } else if (recomd == "2") {
      return soda2 + " " + price[1].toString() + " " + " bath";
    } else if (recomd == "3") {
      return soda3 + " " + price[2].toString() + " " + " bath";
    } else if (recomd == "4") {
      return soda4 + " " + price[3].toString() + " " + " bath";
    }
    print('-------------------------------');
    return "error taobin";
  }

  String menuCoffee() {
    var price = [30, 30, 35, 45];
    var cof1 = 'Coffee';
    var cof2 = 'Black Coffee';
    var cof3 = 'Mocca';
    var cof4 = 'Arabica';
    print('Recommended Menu\n1.' +
        cof1 +
        ' ' +
        price[0].toString() +
        ' bath\n2.' +
        cof2 +
        ' ' +
        price[1].toString() +
        ' bath\n3.' +
        cof3 +
        ' ' +
        price[2].toString() +
        ' bath\n4.' +
        cof4 +
        ' ' +
        price[3].toString() +
        ' bath');
    print('Select your order is: ');
    String recomd = stdin.readLineSync()!;
    if (recomd == "1") {
      return cof1 + " " + price[0].toString() + " " + " bath";
    } else if (recomd == "2") {
      return cof2 + " " + price[1].toString() + " " + " bath";
    } else if (recomd == "3") {
      return cof3 + " " + price[2].toString() + " " + " bath";
    } else if (recomd == "4") {
      return cof4 + " " + price[3].toString() + " " + " bath";
    }
    print('-------------------------------');
    return "error taobin";
  }
}
